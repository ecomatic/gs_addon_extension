<?php

/* Function to hide add-ons in product view based on what tags the
product has. This is to let customers choose extra add-ons or remove
standard add-ons without product price becoming incorrect.
*/

wp_localize_script('gs_addon_correction', 'MyAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
function gs_addon_correction() {
  ?>
  <script type="text/javascript">
    var product_id = 0;

    jQuery(document).ready(function ($) {
      $(document).on('click', '.yith-wcqv-button', function() {
        product_id = $(this).data('product_id');
      });
      $(document).on('qv_loader_stop', function(e) {
        console.log('gs_addon script triggered, product id is ' + product_id);
        var data = {
          action: 'gs_ajax_stuff',
          product_id: product_id,
        };
        $.ajax({
          url: ajaxurl,
          data: data,
          datatype: 'json',
          type: 'POST',
          success: function(response) {
            response = JSON.parse(response);
            var variations = $(".ywapo_input_container_radio");
            variations.each(function() {
              var priceSpan = $(this).children('span');
              if(priceSpan.length != 0) {
                var priceAmount = $(priceSpan.find("span")[0]);
                var price = parseInt(priceAmount.text().split('.')[0]);
                var variationPrice = parseInt(response['price']) + price;
                priceSpan.text(" " + variationPrice + " kr ");
              } else {
                $(this).append("<span class='ywapo_label_price'> " + response['price'] + " kr</span>")
              }
            });
            // $(".ywapo_input_container_radio").append("<span class='woocommerce_price_label'><span class='woocommerce-Price-amount amount'>100<span class='woocommere-Price-currencySymbol'>:-</span></span></span>");

            for (var i = 0; i < response['tags'].length; i++) {
              response['tags'][i] = response['tags'][i].replace(/\s+/g, '').toUpperCase();
            }

            var addonsGroups = jQuery(".ywapo_group_container_checkbox");
            addonsGroups.each(function() {
              var h3 = jQuery(this).find('h3');

              if(h3.html().indexOf('priceless') != -1) {
                h3.hide();
                var theAddons = jQuery(this).find('.ywapo_input_container');

                theAddons.each(function() {
                  var addon = jQuery(this).find('.ywapo_option_label').html().replace(/\s+/g, '').toUpperCase();
                  if(jQuery.inArray(addon, response['tags']) > -1) {
                    jQuery(this).find('input').prop('checked', true);
                  } else {
                    jQuery(this).hide();
                  }
                });

              }

              else {
                var theAddons = jQuery(this).find('.ywapo_input_container');

                theAddons.each(function() {
                  var addon = jQuery(this).find('.ywapo_option_label')[0].innerHTML.replace(/\s+/g, '').toUpperCase();
                  if(jQuery.inArray(addon, response['tags']) > -1) {
                    jQuery(this).hide();
                  }
                });
              }
            }); // End addonsGroups foreach
          }
        });
      });
    });
  </script>
  <?php
}

//TODO: Fix this at later date, ajaxurl etc
// add_action('wp_enqueue_scripts', 'gs_register_script');
// function gs_register_script() {
//   wp_register_script('gs-addon-correction', plugin_dir_path( __FILE__ ) . 'js/gs-ajax.js', array ('jquery'));
// }
//
// add_action('wp_enqueue_scripts', 'gs_enqueue_script');
// function gs_enqueue_script() {
//   wp_enqueue_script('gs-addon-correction')
// }

add_action('wp_ajax_gs_ajax_stuff', 'gs_ajax_stuff');
add_action('wp_ajax_nopriv_gs_ajax_stuff', 'gs_ajax_stuff');
function gs_ajax_stuff() {
  $prod = wc_get_product($_POST['product_id']);
  $data['price'] = $prod->get_price();
  $data['tags'] = explode(', ', strip_tags($prod->get_tags()));
  echo json_encode($data);
  wp_die();
}
