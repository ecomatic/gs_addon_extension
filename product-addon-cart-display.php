<?php

/* Modify how the add-on selections made by the customer are displayed
in cart view.
*/
add_action('woocommerce_cart_contents', 'gs_testing_cart');
add_action('woocommerce_review_order_before_shipping', 'gs_testing_cart');
function gs_testing_cart() {
  $items_array = array();
  foreach(WC()->cart->get_cart() as $cart_item) {
    $item['tags'] = explode(', ', strip_tags(wc_get_product_tag_list($cart_item['data']->get_id())));
    $item['options'] = $cart_item['yith_wapo_options'];
    array_push($items_array, $item);
  }
  ?>
  <script type="text/javascript">

    var correctCart;

    jQuery(document).ready(function($) {
      correctCart = function () {
        var cartItems = $('.cart_item');
        var itemsArray = JSON.parse('<?php echo json_encode($items_array); ?>');
        cartItems.each(function(i) {
          var tags = itemsArray[i]['tags'].slice();
          for (var index = 0; index < tags.length; index++) {
            tags[index] = tags[index].replace(/\s+/g, '').toUpperCase();
          }
          var options = itemsArray[i]['options'];
          var cartItemTd = $(this).children('.product-name');
          cartItemTd.find('.variation').children().each(function() {
            $(this).hide();
          });

          options.forEach(function(option) {
            if(option.name.indexOf('priceless') != -1 && tags.indexOf(option.value.replace(/\s+/g, '').toUpperCase()) != -1) {
              var index = tags.indexOf(option.value.replace(/\s+/g, '').toUpperCase());
              tags.splice(index, 1);
              itemsArray[i]['tags'].splice(index, 1);
            } else {
              if(option.name == 'Variationer') {
                cartItemTd.children('.variation').append('<dt>Storlek: </dt><dd>' + option.value + '</dd>');
              } else {
                cartItemTd.children('.variation').append('<dt> + </dt><dd>' + option.value + '</dd>');
              }
            }
          });
          itemsArray[i]['tags'].forEach(function(tag) {
            cartItemTd.children('.variation').append('<dt> - </dt><dd>' + tag + '</dd>');
          });
        });

        var shippingMethodUl = $('#shipping_method');
        shippingMethodUl.children().each(function() {
          if ($(this).find('input').val() == 'advanced_free_shipping') {
            $(this).prev().hide();
          }
        });
      }

      correctCart();
    });

    jQuery('body').on('updated_cart_totals',function() {
      correctCart();
    });
  </script>
  <?php
}

add_filter('woocommerce_display_item_meta', 'gs_format_item_meta', 10, 3);
function gs_format_item_meta($html, $item, $args) {
  $strOut = '';

  foreach ($item->get_meta_data() as $meta) {
    if($meta->key == '_ywapo_meta_data') {
      $addonsData = $meta->value;
      break;
    }
  }

  if(count($addonsData) != 0) {
    $product_id = $item->get_product_id();
    $terms = get_the_terms($product_id, 'product_tag');
    $printableTags = array();
    $tags = array();

    foreach ($terms as $term) {
      array_push($tags, preg_replace('/\s+/', '', mb_strtoupper($term->name, 'UTF-8')));
      array_push($printableTags, $term->name);
    }

    $strOut .= '<ul class="wc-item-meta">';

    foreach ($addonsData as $addon) {
      if ($addon['add_on_type'] == 'radio') {
        $strOut .= '<li><strong class="wc-item-meta-label">' . $addon['name'] . ':</strong> ' . $addon['value'] . '</li>';
      } else {
        if (strpos($addon['name'], 'priceless')) {
          $key = array_search(preg_replace('/\s+/', '', mb_strtoupper($addon['value'], 'UTF-8')), $tags);
          unset($tags[$key]);
          unset($printableTags[$key]);
        } else {
          $strOut .= '<li><strong class="wc-item-meta-label"> +&nbsp;</strong>' . $addon['value'] . '</li>';
        }
      }
    }

    foreach ($printableTags as $tag) {
      $strOut .= '<li><strong class="wc-item-meta-label"> -&nbsp;&nbsp;</strong>' . $tag . '</li>';
    }

    $strOut .= '</ul>';
  }

  return $strOut;
}