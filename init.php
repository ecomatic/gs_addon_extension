<?php
/*
Plugin Name: GS Product Addon Plugin
Description:
Version: 1.0
Author: Geborek Solutions
Author URI: http://geborek.com
*/

function gspap_options_install() {
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
}

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'gspap_options_install');

include(__DIR__ . '/addon-set-duplication.php');
// include(__DIR__ . '/product-addon-display.php');
include(__DIR__ . '/product-addon-cart-display.php');



/*
add_action('wp_ajax_gs_ajax_call_receptor', 'gs_ajax_call_receptor');
add_action('wp_ajax_nopriv_gs_ajax_call_receptor', 'gs_ajax_call_receptor');
function gs_ajax_call_receptor() {
	$productId = $_POST['product_id'];
	$_product = wc_get_product($productId);
	ob_start();
	var_dump($_product);
	file_put_contents('/var/www/fredos_staging/test', ob_get_clean(), FILE_APPEND);
	wp_die();
}
*/

/* 
function gs_product_modal_link_open() {
	echo "<div class=\"gs-product-modal-link\" data-product-id=\"" . get_the_ID() . "\">";
}
function gs_product_modal_link_close() {
	echo "</div>";
}
add_action('woocommerce_after_shop_loop_item', 'gs_product_modal_link_close', 5);
add_action('woocommerce_before_shop_loop_item', 'gs_product_modal_link_open', 10);
 */

add_action( 'after_setup_theme', 'replace_parent_theme_features', 10 );
function replace_parent_theme_features() {
	remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open' );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close' );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail' );
}

add_action( 'woocommerce_shop_loop_item_title', 'gs_output_tags', 50);
function gs_output_tags() {
  $tags = get_the_terms(get_the_ID(), 'product_tag');
  $tag_array = array();
	if($tags) {
	  foreach ($tags as $tag) {
	    array_push($tag_array, $tag->name);
	  }
	echo "<div class=\"description\">";
	  echo implode(', ', $tag_array);
	echo "</div>";
	}
}