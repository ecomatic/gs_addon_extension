var product_id = 0;

jQuery(document).ready(function ($) {
  $(document).on('click', '.yith-wcqv-button', function() {
    product_id = $(this).data('product_id');
  });
  $(document).on('qv_loader_stop', function(e) {
    console.log('gs_addon script triggered, product id is ' + product_id);
    var data = {
      action: 'gs_ajax_stuff',
      product_id: product_id,
    };
    $.ajax({
      url: MyAjax.ajaxurl,
      data: data,
      datatype: 'json',
      type: 'POST',
      success: function(response) {
        response = JSON.parse(response);
        var variations = $(".ywapo_input_container_radio");
        variations.each(function() {
          var priceSpan = $(this).children('span');
          if(priceSpan.length != 0) {
            var priceAmount = $(priceSpan.find("span")[0]);
            var price = parseInt(priceAmount.text().split('.')[0]);
            var variationPrice = parseInt(response['price']) + price;
            priceSpan.text(" " + variationPrice + " kr ");
          } else {
            $(this).append("<span class='ywapo_label_price'> " + response['price'] + " kr</span>")
          }
        });
        // $(".ywapo_input_container_radio").append("<span class='woocommerce_price_label'><span class='woocommerce-Price-amount amount'>100<span class='woocommere-Price-currencySymbol'>:-</span></span></span>");
        var addonsID = jQuery(".ywapo_group_container_checkbox").attr('data-id');
        var addoffsDiv = jQuery('.ywapo_group_container_checkbox').not('[data-id="' + addonsID + '"]');
        var theAddons = jQuery('.ywapo_group_container_checkbox > .ywapo_input_container');
        addoffsDiv.find('h3').hide();
        addoffsDiv.find('.ywapo_product_option_description').hide();
        theAddons.each(function() {
          var addon = jQuery(this).find('.ywapo_option_label')[0].innerHTML;
          if(jQuery.inArray(addon, response['tags']) > -1 && jQuery(this).parent().attr('data-id') == addonsID) {
            jQuery(this).hide();
          } else if (jQuery(this).parent().attr('data-id') != addonsID) {
            if(jQuery.inArray(addon, response['tags']) > -1) {
              jQuery(this).find('input').prop('checked', true);
            } else {
              jQuery(this).hide();
            }
          }
        });
      }
    });
  });
});
