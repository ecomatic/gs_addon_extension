<?php

//Create menu item for copying add-ons between vendors
add_action('admin_menu', 'gs_addon_menu');
function gs_addon_menu() {
  add_submenu_page(
                'edit.php?post_type=product',
                __( 'Copy Add-Ons', 'gs_addon_duplication' ),
                __( 'Copy Add-Ons', 'gs_addon_duplication' ),
                'manage_options',
                'gs_addon_duplication',
                'gs_addon_duplication'
            );
}
/* This is where the add-ons are duplicated between groups and vendors.
Upon loading this page, priceless versions of the add-on entries are also
created. These are used to let customers deselect standard add-ons without
price change.
*/
function gs_addon_duplication() {
  global $wpdb;
  $groups_db   = $wpdb->prefix . 'yith_wapo_groups';
  $types_db    = $wpdb->prefix . 'yith_wapo_types';
  $taxonomy_db = $wpdb->prefix . 'term_taxonomy';
  $terms_db    = $wpdb->prefix . 'terms';

  if(isset($_POST['refresh'])) {
    $wpdb->query("DELETE FROM $types_db WHERE label LIKE '%-priceless';");

    ?><div class="updated notice">
      <p><?php echo "Priceless add-ons have been refreshed"; ?></p>
    </div><?php
  }
  else if(isset($_POST['rem-dupe'])) {
	  $terms = $wpdb->prefix . 'terms';
	  $taxonomy = $wpdb->prefix . 'term_taxonomy';
	  $term_rel = $wpdb->prefix . 'term_relationships';
	  $termmeta = $wpdb->prefix . 'termmeta';
	  $tags = $wpdb->get_results("SELECT * FROM $terms A LEFT JOIN $taxonomy B ON A.term_id = B.term_id WHERE B.taxonomy = 'product_tag';");
	  $duplicates = array();
	  foreach($tags as $tag) {
		  $currentTag = $tag;
		  unset($tags[array_search($tag, $tags)]);
		  foreach($tags as $secondTag) {
			  if($secondTag->slug == $currentTag->slug) {
				  $duplicates[$currentTag->slug] = array($currentTag, $secondTag);
				  unset($tags[array_search($secondTag, $tags)]);
			  }
		  }
	  }
	  
	  $removedDupes = array();
	  
	  foreach($duplicates as $pair) {
		  if($pair[0]->count > $pair[1]->count) {
			  $wpdb->update($term_rel, array('term_taxonomy_id' => $pair[0]->term_taxonomy_id), array('term_taxonomy_id' => $pair[1]->term_taxonomy_id));
			  $wpdb->delete($taxonomy, array('term_id' => $pair[1]->term_id));
			  $wpdb->delete($termmeta, array('term_id' => $pair[1]->term_id));
			  $wpdb->delete($terms, array('term_id' => $pair[1]->term_id));
		  } else {
			  $wpdb->update($term_rel, array('term_taxonomy_id' => $pair[1]->term_taxonomy_id), array('term_taxonomy_id' => $pair[0]->term_taxonomy_id));
			  $wpdb->delete($taxonomy, array('term_id' => $pair[0]->term_id));
			  $wpdb->delete($termmeta, array('term_id' => $pair[0]->term_id));
			  $wpdb->delete($terms, array('term_id' => $pair[0]->term_id));
		  }
	  }
	  ?><div class="updated notice">
      <p><?php echo "Duplicate tags have been removed."; ?></p>
	</div><?php
  }
  else if(isset($_POST['submit'])) {
    $origin_group   = $_POST['gs-origin-group'];
    $target_vendor  = $_POST['gs-target-vendor'];
    $name           = $_POST['gs-new-group-name'];

    $wpdb->insert($groups_db, array(
      'name'        => $name,
      'user_id'     => $target_vendor,
      'visibility'  => 9
    ));
    $group_id = $wpdb->insert_id;

    $types = $wpdb->get_results("SELECT * FROM $types_db WHERE group_id = '$origin_group' AND label NOT LIKE '%-priceless';");
    foreach ($types as $type) {
      $row = (array) $type;
      unset($row['id']);
      $row['group_id'] = $group_id;
      $wpdb->insert($types_db, $row);

      if($row['type'] == 'checkbox') {
        $new_type = $wpdb->get_row("SELECT * FROM $types_db WHERE id = '$wpdb->insert_id'");
        gs_make_priceless($new_type);
      }
    }
    ?>

    <div class="updated notice">
      <p><?php echo "$origin_group was copied to $target_vendor as $name"; ?></p>
    </div>

    <?php
  }

  $vendor_taxonomies = $wpdb->get_results("SELECT * FROM $taxonomy_db WHERE taxonomy = 'yith_shop_vendor';");
  $terms_query = "SELECT * FROM $terms_db WHERE term_id = '0'";
  foreach ($vendor_taxonomies as $tax) {
    $terms_query .= " OR term_id = '" . $tax->term_id . "'";
  }

  $vendors = $wpdb->get_results($terms_query);
  $groups = $wpdb->get_results("SELECT * FROM $groups_db WHERE del <> 1");
  $types_priceless = $wpdb->get_results("SELECT * FROM $types_db WHERE type = 'checkbox' AND label LIKE '%-priceless';");
  $types = $wpdb->get_results("SELECT * FROM $types_db WHERE del <> 1 AND type = 'checkbox' AND label NOT LIKE '%-priceless';");

  foreach ($types as $type) {
    if(!gs_has_priceless($type, $types_priceless)) {
      gs_make_priceless($type);
    }
  }

  ?>
  <div class="wrapper">
    <form id="gs-addon-form" class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
      <table class="wp-list-table widefat fixed striped posts">
        <tbody>
          <tr>
            <th style="width:10%;"><strong>Group to copy</strong></th>
            <td>
              <select class="" name="gs-origin-group">
              <?php foreach ($groups as $group) { ?>
                <option value="<?php echo $group->id ?>" <?php echo (isset($_POST['gs-origin-group']) && $_POST['gs-origin-group'] == $group->id ? 'selected' : '') ?>><?php echo $group->name; ?></option>
              <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <th><strong>New group vendor</strong></th>
            <td>
              <select class="" name="gs-target-vendor">
              <?php foreach ($vendors as $vendor) { ?>
                <option value="<?php echo $vendor->term_id ?>" <?php echo (isset($_POST['gs-target-vendor']) && $_POST['gs-target-vendor'] == $vendor->term_id ? 'selected' : '') ?>><?php echo $vendor->name; ?></option>
              <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <th><strong>New group name</strong></th>
            <td>
              <input type="text" name="gs-new-group-name" value="">
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <input type="submit" name="submit" value="Create group" class="button">
              <input type="submit" name="refresh" value="Refresh priceless" class="button">
              <input type="submit" name="rem-dupe" value="Remove duplicate tags" class="button">
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  </div>
  <?php
}

/* Check if the current set of add-ons $type has a priceless counterpart in the
$priceless array.
*/
function gs_has_priceless($type, $priceless) {
  $typestring = $type->id . '-priceless';
  foreach ($priceless as $key => $value) {
    if($typestring == $value->label) {
      return true;
    }
  }
  return false;
}

//Create a priceless version of a set of add-ons
function gs_make_priceless($type) {
  global $wpdb;
  $row = (array) $type;
  $options = maybe_unserialize($type->options);
  if(isset($options['price'])) {
    foreach ($options['price'] as $key => $value) {
      $options['price'][$key] = 0;
    }
  }
  $row['options'] = serialize($options);
  unset($row['id']);
  $row['label'] = $type->id . '-priceless';
  $wpdb->insert($wpdb->prefix . 'yith_wapo_types', $row);
  return $wpdb->insert_id;
}